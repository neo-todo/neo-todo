
import { Injectable,Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/service/auth.service';
import { Repository } from 'typeorm';
import { UserEntity } from '../models/user.entity';
import { CreateUserDto, User } from '../models/user.interface';


@Injectable()
export class UserService {
    constructor(
        @InjectRepository(UserEntity) private readonly userRepository: Repository<UserEntity>,
        private authService:AuthService
    ){}

    create(user:CreateUserDto):Promise<User>{
        return this.authService.hashPassword(user.password).then(((passwordHash:string)=>{
                const newUser = new UserEntity();
                newUser.name = user.name;
                newUser.username = user.username;
                newUser.email = user.email;
                newUser.password = passwordHash;

                return this.userRepository.save(newUser).then(((user:User)=>{
                        const {password, ...result} = user;
                        return result;
                    })
                );
            })
        ) 
    }

    findOne(id:number):Promise<User>{
        return this.userRepository.findOneBy({id}).then((user:User)=>{
            const {password, ...result} = user;
            return result
        })
    }

    findAll():Promise<User[]>{
        return this.userRepository.find().then((users:User[])=>{
            users.forEach(function(v) {delete v.password});
            return users;
        })
    }

    deleteOne(id:number):Promise<any>{
        return this.userRepository.delete(id)
    }

    updateOne(id:number, user:User):Promise<any>{

        delete user.email,
        delete user.password

        return this.userRepository.update(id,user)
    }
  

    login(user:User):Promise<string>{
        return this.validateUser(user.email, user.password).then(((user:User)=>{
                if(user){
                    return this.authService.generateJWT(user).then(((jwt:string)=>jwt))
                } else{
                    return "Wrong Credentials"
                }
            })
        )

    }
    validateUser(email:string, password:string):Promise<User>{
        return this.findByMail(email).then(((user:User)=> this.authService.comparePasswords(password, user.password).then(
                ((match:boolean)=>{
                    if(match){
                        const{password, ...result} = user;
                        return result;
                    } else{
                        throw Error 
                    }
                })
            ))
        )

    }

    findByMail(email:string):Promise<User>{
        return this.userRepository.findOneBy({email});//with new versions of typeorm we should use findOneBy instead findOne

    }

}


//Same in Observable. Should not be done, waste of time and more lines of code
/*
    findOne(id:number):Observable<User>{
        return from(this.userRepository.findOneBy({id})).pipe(
            map((user:User)=>{
                const {password, ...result} = user;
                return result
            })
        );//check if works
    }*/
/*
    findOne(id:number):Observable<User>{
        return from(this.userRepository.findOneBy({id})).pipe(
            map((user:User)=>{
                const {password, ...result} = user;
                return result
            })
        );//check if works
    }

    findAll():Observable<User[]>
    {
        return from(this.userRepository.find()).pipe(
            map((users:User[])=>{
                users.forEach(function(v) {delete v.password});
                return users;
            })
        )
    }

    deleteOne(id: number):Observable<any>{
        return from(this.userRepository.delete(id))
    }

    updateOne(id: number, user:User):Observable<any>{
        delete user.email;
        delete user.password;

        return from(this.userRepository.update(id,user))//we search by id and we update  user
    }

    login(user:User):Observable<string>{
        return this.validateUser(user.email, user.password).pipe(
            switchMap((user:User)=>{
                if(user){
                    return this.authService.generateJWT(user).pipe(map((jwt:string)=>jwt))
                } else{
                    return "Wrong Credentials"
                }
            })
        )

    }*/

    /*
    validateUser(email:string, password:string):Observable<User>{
        return this.findByMail(email).pipe(
            switchMap((user:User)=> this.authService.comparePasswords(password, user.password).pipe(
                map((match:boolean)=>{
                    if(match){
                        const{password, ...result} = user;
                        return result;
                    } else{
                        throw Error 
                    }
                })
            ))
        )

    }

    findByMail(email:string):Observable<User>{
        return from(this.userRepository.findOneBy({email}));//with new versions of typeorm we should use findOneBy instead findOne

    }*/

    /*
    create(user:CreateUserDto):Observable<User>{
        return this.authService.hashPassword(user.password).pipe(
            switchMap((passwordHash:string)=>{
                const newUser = new UserEntity();
                newUser.name = user.name;
                newUser.username = user.username;
                newUser.email = user.email;
                newUser.password = passwordHash;

                return from(this.userRepository.save(newUser)).pipe(
                    map((user:User)=>{
                        const {password, ...result} = user;
                        return result;
                    })
                );
            })
        ) 
    }*/