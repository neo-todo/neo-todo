import { Exclude } from "class-transformer";
import {IsEmail, IsString} from "class-validator"


export class User {
    id?: number;
    name?: string;
    username?: string;
    email?:string;

    @Exclude()//Password will be hashed and stored in DB but not exposed while posting in postman or browser
    password?: string;
}



export class CreateUserDto{
    @IsString()
    name: string;

    @IsString()
    username: string;

    @IsEmail()
    email:string;

    @IsString()
    password: string;

}


//Class, Not Interace?????