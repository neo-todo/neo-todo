//this is how we strore in DB

import { BeforeInsert, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class UserEntity {

    @PrimaryGeneratedColumn()//Creates a primary column which value will be auto generated with auto-increment value
    id:number;

    @Column()
    name: string;

    @Column({unique:true})//One user name for every user so this one should be unique 
    username: string;

    @Column()
    email:string;

    @Column()
    password:string;
    
    @BeforeInsert()//we post mail always as lowercase in database
    emailToLowerCase(){
        this.email=this.email.toLowerCase();
    }
}