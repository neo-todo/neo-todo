import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getIntroduction(): string {
    return 'This is BE for TodoList (Neo)!';
  }
}
