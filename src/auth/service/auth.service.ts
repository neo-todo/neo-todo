import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { of,from, Observable } from 'rxjs';
import { User } from 'src/user/models/user.interface';
const bcrypt = require('bcrypt')

@Injectable()
export class AuthService {
    constructor(private readonly jwtSecret:JwtService){}

    generateJWT(user:User):Promise<string>{
        return this.jwtSecret.signAsync({user}) //generator JWT
    }

    hashPassword(password:string):Promise<string>{
        return bcrypt.hash(password,12) //hash password
    }

    comparePasswords(newPassword:string, passwordHash:string ):Promise<any | boolean >{
        return (bcrypt.compare(newPassword,passwordHash))
    }
}
